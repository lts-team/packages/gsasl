@subheading gsasl_md5
@anchor{gsasl_md5}
@deftypefun {int} {gsasl_md5} (const char * @var{in}, size_t @var{inlen}, char * @var{out[16]})
@var{in}: input character array of data to hash.

@var{inlen}: length of input character array of data to hash.

Compute hash of data using MD5.  The @code{out} buffer must be
deallocated by the caller.

@strong{Return value:} Returns @code{GSASL_OK} iff successful.
@end deftypefun

