@subheading gsasl_nonce
@anchor{gsasl_nonce}
@deftypefun {int} {gsasl_nonce} (char * @var{data}, size_t @var{datalen})
@var{data}: output array to be filled with unpredictable random data.

@var{datalen}: size of output array.

Store unpredictable data of given size in the provided buffer.

@strong{Return value:} Returns @code{GSASL_OK} iff successful.
@end deftypefun

@subheading gsasl_random
@anchor{gsasl_random}
@deftypefun {int} {gsasl_random} (char * @var{data}, size_t @var{datalen})
@var{data}: output array to be filled with strong random data.

@var{datalen}: size of output array.

Store cryptographically strong random data of given size in the
provided buffer.

@strong{Return value:} Returns @code{GSASL_OK} iff successful.
@end deftypefun

@subheading gsasl_md5
@anchor{gsasl_md5}
@deftypefun {int} {gsasl_md5} (const char * @var{in}, size_t @var{inlen}, char * @var{out[16]})
@var{in}: input character array of data to hash.

@var{inlen}: length of input character array of data to hash.

Compute hash of data using MD5.  The @code{out} buffer must be
deallocated by the caller.

@strong{Return value:} Returns @code{GSASL_OK} iff successful.
@end deftypefun

@subheading gsasl_hmac_md5
@anchor{gsasl_hmac_md5}
@deftypefun {int} {gsasl_hmac_md5} (const char * @var{key}, size_t @var{keylen}, const char * @var{in}, size_t @var{inlen}, char * @var{outhash[16]})
@var{key}: input character array with key to use.

@var{keylen}: length of input character array with key to use.

@var{in}: input character array of data to hash.

@var{inlen}: length of input character array of data to hash.

Compute keyed checksum of data using HMAC-MD5.  The @code{outhash} buffer
must be deallocated by the caller.

@strong{Return value:} Returns @code{GSASL_OK} iff successful.
@end deftypefun

@subheading gsasl_sha1
@anchor{gsasl_sha1}
@deftypefun {int} {gsasl_sha1} (const char * @var{in}, size_t @var{inlen}, char * @var{out[20]})
@var{in}: input character array of data to hash.

@var{inlen}: length of input character array of data to hash.

Compute hash of data using SHA1.  The @code{out} buffer must be
deallocated by the caller.

@strong{Return value:} Returns @code{GSASL_OK} iff successful.

@strong{Since:} 1.3
@end deftypefun

@subheading gsasl_hmac_sha1
@anchor{gsasl_hmac_sha1}
@deftypefun {int} {gsasl_hmac_sha1} (const char * @var{key}, size_t @var{keylen}, const char * @var{in}, size_t @var{inlen}, char * @var{outhash[20]})
@var{key}: input character array with key to use.

@var{keylen}: length of input character array with key to use.

@var{in}: input character array of data to hash.

@var{inlen}: length of input character array of data to hash.

Compute keyed checksum of data using HMAC-SHA1.  The @code{outhash} buffer
must be deallocated by the caller.

@strong{Return value:} Returns @code{GSASL_OK} iff successful.

@strong{Since:} 1.3
@end deftypefun

