@subheading gsasl_sha1
@anchor{gsasl_sha1}
@deftypefun {int} {gsasl_sha1} (const char * @var{in}, size_t @var{inlen}, char * @var{out[20]})
@var{in}: input character array of data to hash.

@var{inlen}: length of input character array of data to hash.

Compute hash of data using SHA1.  The @code{out} buffer must be
deallocated by the caller.

@strong{Return value:} Returns @code{GSASL_OK} iff successful.

@strong{Since:} 1.3
@end deftypefun

